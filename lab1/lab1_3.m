clear all;
close all;
clc;

signal = "music";
Nbits = [4 6 8];
pe = logspace(-9,-1,9);
algorithm1 = "mu";
algorithm2 = "lloyd";
compandingParameter = 255;
lloydTrainingSetMaxLength = 1e4;

load(signal);
lloydTrainingSet = v_in(1:min(length(v_in),lloydTrainingSetMaxLength));
[~,SNR1] = pcm(v_in,Nbits,V,pe,algorithm1,compandingParameter,NaN);
[~,SNR2] = pcm(v_in,Nbits,V,pe,algorithm2,NaN,lloydTrainingSet);
snrGraphTwoAlgorithms(signal,algorithm1,algorithm2,Nbits,pe,SNR1,SNR2,loss);

signal = "voice";

load(signal);
lloydTrainingSet = v_in(1:min(length(v_in),lloydTrainingSetMaxLength));
[~,SNR1] = pcm(v_in,Nbits,V,pe,algorithm1,compandingParameter,NaN);
[~,SNR2] = pcm(v_in,Nbits,V,pe,algorithm2,NaN,lloydTrainingSet);
snrGraphTwoAlgorithms(signal,algorithm1,algorithm2,Nbits,pe,SNR1,SNR2,loss);