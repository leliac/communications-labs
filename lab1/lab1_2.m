clear all;
close all;
clc;

signal = "music";
musicFile = "corelli.mp3";
musicChannel = 1;
Nsamples = 1e6;
Nbits = [4 6 8];
pe = logspace(-9,-1,9);
algorithm = "uniform";
Nbins = 100;

[v_in,V,loss,~] = signalGenerator(signal,Nsamples,NaN,NaN,musicFile,musicChannel,NaN,Nbins);
[~,SNR] = pcm(v_in,Nbits,V,pe,algorithm,NaN,NaN);
snrGraph(signal,algorithm,Nbits,pe,SNR,loss);

signal = "voice";
fc = 44100;
voiceBitsPerSample = 24;

[v_in,V,loss,~] = signalGenerator(signal,Nsamples,NaN,fc,"",NaN,voiceBitsPerSample,Nbins);
[~,SNR] = pcm(v_in,Nbits,V,pe,algorithm,NaN,NaN);
snrGraph(signal,algorithm,Nbits,pe,SNR,loss);