clear all;
close all;
clc;

signal = "uniform";
Nsamples = 1e6;
A = 1;
fc = 44100;
Nbits = [4 6 8];
pe = logspace(-9,-1,9);
algorithm = "uniform";
Nbins = 100;

[v_in,V,~,~] = signalGenerator(signal,Nsamples,A,fc,"",NaN,NaN,Nbins);
[~,SNR] = pcm(v_in,Nbits,V,pe,algorithm,NaN,NaN);
snrGraph(signal,algorithm,Nbits,pe,SNR,NaN);