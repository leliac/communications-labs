clc;
close all;
clear all;

%% Simulation parameters
inputFile = 'lab3_2rx'; % input file
samplesPerPsd = 2^7; % samples per PSD
outputImage = sprintf('%s.svg',mfilename); % output image

%% Initialization
load(inputFile);
f = NaN(samplesPerPsd,numCenterFreqs); % PSD's frequencies
psd = NaN(samplesPerPsd,numCenterFreqs); % PSD's
chFreqs = 1e6*[174.928 176.640 178.352 180.064 181.936 183.648 185.360 187.072 188.928 190.640 192.352 194.064 195.936 197.648 199.360 201.072 202.928 204.640 206.352 208.064 209.936 211.648 213.360 215.072 216.928 218.640 220.352 222.064 223.936 225.648 227.360 229.072 230.784 232.496 234.208 235.776 237.488 239.200]; % channel frequencies
chNames = {'5A','5B','5C','5D','6A','6B','6C','6D','7A','7B','7C','7D','8A','8B','8C','8D','9A','9B','9C','9D','10A','10B','10C','10D (CRDAB Piemonte)','11A','11B','11C','11D','12A (EuroDAB Italia)','12B (DAB+ RAI)','12C (DAB Italia)','12D (Radiofonia locale digitale Torino-Cuneo)','13A','13B','13C','13D','13E','13F'}; % channel and MUX names

%% Spectrum evaluation
for k = 1:numCenterFreqs
    [f(:,k),psd(:,k)] = averagedPsd(samples(:,k),fC,centerFreqs(k),samplesPerPsd,false,inf,inf,'','');
end
offset = samplesPerPsd/2*(1-dB/fC);
first = 1+offset;
last = samplesPerPsd-offset;
samplesPerPsd = 1+last-first;
f = f(first:last,:);
psd = psd(first:last,:);
f = reshape(f,samplesPerPsd*numCenterFreqs,1);
psd = reshape(psd,samplesPerPsd*numCenterFreqs,1);

%% Spectrum graph
kMin = 1;
while kMin <= length(chFreqs) && chFreqs(kMin) < f(1)
    kMin = kMin+1;
end
kMax = kMin;
while kMax <= length(chFreqs) && chFreqs(kMax) <= f(end)
    kMax = kMax+1;
end
kMax = kMax-1;
figure;
semilogy(1e-6*f,psd);
for k = kMin:kMax
    chLine = xline(1e-6*chFreqs(k),'-',chNames{k});
    chLine.Color = [0.8500, 0.3250, 0.0980];
end
for k = 1:numCenterFreqs
    xline(1e-6*centerFreqs(k),'--');
end
xlim(1e-6*[min(f) max(f)]);
ylim([min(psd) max(psd)]);
title('DAB spectrum');
xlabel('f [MHz]');
ylabel('PSD');
set(gcf,'units','normalized','outerposition',[0 0 1 1]);
saveas(gcf,outputImage);