clc;
close all;
clear all;

%% Simulation parameters
debug = 0; % generate fake samples instead of receiving real ones
fC = 2e6; % sample rate (acquisition bandwidth)
dB = fC/2; % distance between center frequencies
freqMin = 214e6; % minimum frequency
freqMax= 230e6; % maximum frequency
samplesPerFrame = 2^18; % samples per frame
agc = false; % active gain control
tunerGain = 30; % tuner gain (only used if active gain control is disabled)
outputDataType = 'single'; % output data type
framesPerCenterFreq = 2^7; % frames per center frequency
outputFile = mfilename; % output file

%% Initialization
centerFreqs = freqMin+dB/2:dB:freqMax-dB/2; % center frequencies
numCenterFreqs = length(centerFreqs); % number of center frequencies
if ~debug
    radio = comm.SDRRTLReceiver(...
        'SampleRate',fC,...
        'SamplesPerFrame',samplesPerFrame,...
        'EnableTunerAGC',agc,...
        'TunerGain',tunerGain,...
        'OutputDataType',outputDataType); % SDR device
    if isempty(sdrinfo(radio.RadioAddress))
        error('unable to find SDR device');
    end
    lost = NaN(framesPerCenterFreq,1); % sample loss flags
end
samplesPerCenterFreq = framesPerCenterFreq*samplesPerFrame; % samples per center frequency
samples = single(NaN(samplesPerCenterFreq,numCenterFreqs)); % samples

%% Signal acquisition
if ~debug
    for k = 1:numCenterFreqs
        radio.CenterFrequency = centerFreqs(k);
        for l = 1:framesPerCenterFreq
             [data,~,lost(l)] = step(radio);
             samples(1+(l-1)*samplesPerFrame:l*samplesPerFrame,k) = data-mean(data);
        end
        sumLost = sum(lost);
        if sumLost ~= 0
            warning('lost %d samples between frames',sumLost);
        end
    end
    release(radio);
else
    for k = 1:numCenterFreqs
        [samples(:,k),~] = bbDigitalTx(randi([0 1],samplesPerCenterFreq/2^4,1),fC,fC/2^4,1,'gray','antipodal','nrz');
    end
end
save(outputFile,'-v7.3');