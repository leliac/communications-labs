%% Simulation parameters
debug = true; % read samples from audio file
Nints = 18; % number of integers
trailSeq = [0 1 1 0 1 1 1 0 0 1 0 1 1 1 0 1 1 1 1 0 0 0]'; % trailing sequence
BpInt = 8; % bits per integer
BpS = 1; % bits per symbol
mapping = "gray"; % symbol mapping ('std','gray' or user-defined matrix)
symbols = "unipolar"; % symbols ('antipodal', 'unipolar' or user-defined vector)
pulse = "nrz"; % pulse ('nrz' or user-defined function)
Rb = 100; % bit rate
fc = 16000; % sample rate
f0 = 2000; % modulation frequency
BpSamp = 16; % audio card bits per sample
audioFile = 'lab3_1tx.wav'; % input audio file (only used in debug mode)
tRec = 10; % recording duration
filter = "sinc"; % filter ('sinc', 'RC' or user-defined function)
fp = 50; % RC filter frequency (only used with RC filter)
A = 2*(2^BpS-1); % filter scaling factor
tSample = "optimal"; % sampling instant ('opt' or user-defined scalar)
thresholds = "centered"; % thresholds ('centered' or user-defined vector)

%% Initialization
SpS = fc/Rb*BpS; % samples per symbol
M = 2^BpS; % PAM order
Nbits = Nints*BpInt; % number of bits
Rs = Rb/BpS; % symbol rate
[ref,~] = bbDigitalTx(trailSeq,fc,Rb,BpS,mapping,symbols,pulse); % reference signal samples
ref = iqModulator(fc,ref,0,f0);
Nsamples = Nbits*fc/Rb+length(ref); % number of samples
recorder = audiorecorder(fc,BpSamp,1); % recorder object

%% Audio card RX
if debug
    [x_rx,fc] = audioread(audioFile); % received samples
else
    disp('starting to receive signal');
    recordblocking(recorder,tRec);
    disp('finished receiving signal');
    x_rx = getaudiodata(recorder); % received samples
end

%% Synchronizer
[~,~,tDelay] = alignsignals(ref,x_rx); % delay between received signal and reference signal
x_rx = x_rx(1+tDelay:tDelay+Nsamples);

%% Demodulator
[x_rx,~] = iqDemodulator(fc,x_rx,f0);
averagedPsd(x_rx,fc,0,Nsamples,true,[-2*f0-2*Rb 2*f0+2*Rb],inf,{'PSD at demodulator';sprintf('(%d-PAM, %s symbols)',M,symbols)},sprintf('%s_01_demodulator_psd_%dpam_%s.svg',mfilename,M,symbols));

%% Baseband digital RX
[bits_rx,alphas_rx,x_rx] = bbDigitalRx(x_rx,fc,Rb,BpS,filter,fp,A,tSample,mapping,symbols,thresholds); % received bits, received symbols
signalVsTime(x_rx,fc,inf,{'Signal at baseband digital RX ';sprintf('(%d-PAM, %s symbols, %s filter)',M,symbols,filter)},sprintf('%s_02_bbDigitalRx_sig_%dpam_%s_%s.svg',mfilename,M,symbols,filter));
eyeDiagram(x_rx,3,SpS,[-1 SpS],[min(x_rx) max(x_rx)],{'Eye diagram at baseband digital RX';sprintf('(%d-PAM, %s symbols, %s filter)',M,symbols,filter)},sprintf('%s_03_bbDigitalRx_eye_%dpam_%s_%s.svg',mfilename,M,symbols,filter));
averagedPsd(x_rx,fc,0,Nsamples,true,[-2*Rb 2*Rb],inf,{'PSD at baseband digital RX';sprintf('(%d-PAM, %s symbols, %s filter)',M,symbols,filter)},sprintf('%s_04_bbDigitalRx_psd_%dpam_%s_%s.svg',mfilename,M,symbols,filter));

%% Data extractor
ints_rx = bi2de(reshape(bits_rx(1+length(trailSeq):end),BpInt,Nints)','left-msb')' % received integers