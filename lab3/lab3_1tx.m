%% Simulation parameters
debug = true; % add silence and noise to output audio file
trailSeq = [0 1 1 0 1 1 1 0 0 1 0 1 1 1 0 1 1 1 1 0 0 0]'; % trailing sequence
ints_tx = [2 0 3 9 4 3 2 3 4 7 0 8 2 3 7 6 6 2]; % transmitted integers
BpInt = 8; % bits per integer
BpS = 1; % bits per symbol
mapping = "gray"; % symbol mapping ('std','gray' or user-defined matrix)
symbols = "unipolar"; % symbols ('antipodal', 'unipolar' or user-defined vector)
pulse = "nrz"; % pulse ('nrz' or user-defined function)
Rb = 100; % bit rate
fc = 16000; % sample rate
f0 = 2000; % modulation frequency
BpSamp = 16; % audio card bits per sample
SNR = 40; % signal to noise ratio in dB (only used in debug mode)
audioFile = sprintf('%s.wav',mfilename); % output audio file

%% Initialization
SpS = fc/Rb*BpS; % samples per symbol
M = 2^BpS; % PAM order
Nints = length(ints_tx); % number of integers

%% Data source
bits_tx = reshape(de2bi(ints_tx,BpInt,'left-msb')',BpInt*Nints,1); % transmitted bits
bits_tx = [trailSeq;bits_tx];

%% Baseband digital TX
[x_tx,symbols_tx] = bbDigitalTx(bits_tx,fc,Rb,BpS,mapping,symbols,pulse); % transmitted samples, transmitted symbols
Nsamples = length(x_tx); % number of samples
signalVsTime(x_tx,fc,inf,{'Signal at baseband digital TX';sprintf('(%d-PAM, %s symbols)',M,symbols)},sprintf('%s_01_bbDigitalTx_sig_%dpam_%s.svg',mfilename,M,symbols));
eyeDiagram(x_tx,3,SpS,[-1 SpS],[min(x_tx) max(x_tx)],{'Eye diagram at baseband digital TX ';sprintf('(%d-PAM, %s symbols)',M,symbols)},sprintf('%s_02_bbDigitalTx_eye_%dpam_%s.svg',mfilename,M,symbols));
averagedPsd(x_tx,fc,0,Nsamples,true,[-2*Rb 2*Rb],inf,{'PSD at baseband digital TX';sprintf('(%d-PAM, %s symbols)',M,symbols)},sprintf('%s_03_bbDigitalTx_psd_%dpam_%s.svg',mfilename,M,symbols));

%% Modulator
x_tx = iqModulator(fc,x_tx,0,f0);
averagedPsd(x_tx,fc,0,Nsamples,true,[-f0-2*Rb f0+2*Rb],inf,{'PSD at modulator';sprintf('(%d-PAM, %s symbols)',M,symbols)},sprintf('%s_04_modulator_psd_%dpam_%s.svg',mfilename,M,symbols));

%% Audio card TX
if debug
    x_tx = [zeros(100,1);x_tx;zeros(1000,1)];
    n = sqrt(mean(abs(x_tx).^2)*SpS/2/BpS./db2pow(SNR))*randn(length(x_tx),1); % noise samples
    x_tx = x_tx+n;
end
x_tx = x_tx/max(abs(max(x_tx)),abs(min(x_tx)));
audiowrite(audioFile,x_tx,fc);
sound(x_tx,fc,BpSamp);