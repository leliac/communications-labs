%% Simulation parameters
Nbits = 1e7;
Rb = 100e6;
% BpS = 2;
SpS = 9;
mapping = "gray";
symbols = "antipodal";
pulse = "nrz";
% pulse = @(t) triangularPulse(0,BpS/Rb,t);
noise = true;
SNR = 1:1:20;
filterMatched = "sinc";
% filterMatched = @(f) sinc(f*BpS/Rb/2).^2;
fp = 1e6*[50 100 150];
tSampMatched = "optimal";
% tSampMatched = 5;
tSampRc = "optimal";
% tSampRc = 5;
Amatched = 1;
% Amatched = 1.478043363768285;
Arc = 1;
% Arc = 1.164896296105722;
thresholds = "centered";
BERtarg = 1e-3;
fitFunc = fittype('a*erfc(real(b*x^c))');
fitFuncInv = @(a,b,c,y) (erfcinv(y/a)/b).^(1/c);

%% Initialization
Rs = Rb/BpS;
fc = SpS*Rs;
M = 2^BpS;
if ~noise
    SNR = inf;
    n = 0;
end
NSNR = length(SNR);
NfP = length(fp);
Nsymbols = Nbits/BpS;
Nsamples = Nsymbols*SpS;
SER_matched = NaN(1,NSNR);
BER_matched = NaN(1,NSNR);
BER_rc = NaN(NfP,NSNR);
BER_rc_fit = cell(1,NfP);
SNRtarg_matched = pow2db(erfcinv(BERtarg*M*BpS/(M-1))^2*(M^2-1)/3/BpS);
SNRtarg_rc = NaN(1,NfP);
SNR_th = linspace(min(SNR),max(SNR),1e3);
SER_matched_th = (M-1)/M*erfc(sqrt(3*BpS*db2pow(SNR_th)/(M^2-1)));
BER_matched_th = SER_matched_th/BpS;
labels = cell(1,3+3*NfP);
lab = 1;
Neyes = 3;
eyeLimX = [-1 SpS];
if symbols == "antipodal"
    eyeLimY = [-(M-1)-0.5 M-1+0.5];
elseif symbols == "unipolar"
    eyeLimY = [0-0.25 (M-1)+0.25];
end

%% Pattern generator
bits_tx = single(randi([0 1],[Nbits 1]));

%% Digital transmitter
[samples_tx,symbols_tx] = bbDigitalTx(bits_tx,fc,Rb,BpS,mapping,symbols,pulse);
samples_tx = single(samples_tx);
symbols_tx = single(symbols_tx);
eyeDiagram(samples_tx,Neyes,SpS,eyeLimX,eyeLimY,{'Eye diagram at TX';sprintf('(%d-PAM)',M)},sprintf('tx_eye_%dpam.svg',M));
standardPsd(samples_tx,1e-6*fc,0,true,[-1e-6*SpS*Rb/2 1e-6*SpS*Rb/2],inf,{'PSD at TX';sprintf('(%d-PAM)',M)},sprintf('tx_psd_%dpam.svg',M));

%%
for m = 1:NSNR
    %% AWGN channel
    if noise
        sn = sqrt(mean(abs(samples_tx).^2)*SpS/2/BpS./db2pow(SNR(m)));
        n = single(sn*randn(Nsamples,1));
    end
    samples_rx = samples_tx+n;
    clear n;

    %% Digital receiver
    [bits_rx_matched,symbols_rx_matched,y_matched] = bbDigitalRx(samples_rx,fc,Rb,BpS,filterMatched,inf,Amatched,tSampMatched,mapping,symbols,thresholds);
    bits_rx_matched = single(bits_rx_matched);
    symbols_rx_matched = single(symbols_rx_matched);
    bits_rx_rc = single(NaN(Nbits,NfP));
    y_rc = single(NaN(Nsamples,NfP));
    for k = 1:NfP
        [bits_rx_rc(:,k),~,y_rc(:,k)] = bbDigitalRx(samples_rx,fc,Rb,BpS,"rc",fp(k),Arc,tSampRc,mapping,symbols,thresholds);
    end
    bits_rx_rc = single(bits_rx_rc);
    clear samples_rx;
    if m == NSNR
        eyeDiagram(y_matched,Neyes,SpS,eyeLimX,eyeLimY,{'Eye diagram at RX';sprintf('(%d-PAM, matched filter, E_b/N_0 = %ddB)',M,SNR(m))},sprintf('rx_eye_matched_snr%d_%dpam.svg',SNR(m),M));
        clear y_matched;
        for k = 1:NfP
            eyeDiagram(y_rc(:,k),Neyes,SpS,eyeLimX,eyeLimY,{'Eye diagram at RX';sprintf('(%d-PAM, %dMHz RC filter, E_b/N_0 = %ddB)',M,fp(k)/1e6,SNR(m))},sprintf('rx_eye_rc%d_snr%d_%dpam.svg',fp(k)/1e6,SNR(m),M));
        end
        clear y_rc;
    end
   
    %% Error counter
    SER_matched(m) = sum(symbols_rx_matched~=symbols_tx)/Nsymbols;
    clear symbols_rx_matched;
    BER_matched(m) = sum(bits_rx_matched~=bits_tx)/Nbits;
    clear bits_rx_matched;
    for k = 1:NfP
        BER_rc(k,m) = sum(bits_rx_rc(:,k)~=bits_tx)/Nbits;
    end
    clear bits_rx_rc;
end
clear bits_tx symbols_tx samples_tx;

if noise
    %% RC filter penalty
    for k = 1:NfP
        BER_rc_fit{k} = fit(db2pow(SNR)',BER_rc(k,:)',fitFunc,'startpoint',[(M-1)/M/BpS sqrt(3*BpS/(M^2-1)) 0.5]);
        SNRtarg_rc(k) = pow2db(fitFuncInv(BER_rc_fit{k}.a,BER_rc_fit{k}.b,BER_rc_fit{k}.c,BERtarg));
    end
    SNRtarg_penalty = SNRtarg_rc-SNRtarg_matched

    %% BER: matched vs. RC filter
    figure;
    hold on;
    yline(log10(BERtarg),'k--');
    labels{lab} = 'target BER';
    lab = lab+1;
    pl = plot(SNR_th,log10(BER_matched_th));
    labels{lab} = 'BER, matched filter (theory)';
    lab = lab+1;
    xline(SNRtarg_matched,'--','color',pl.Color);
    labels{lab} = 'target E_b/N_0, matched filter (theory)';
    lab = lab+1;
    for k = 1:NfP
        pl = plot(SNR,log10(BER_rc(k,:)),'o');
        labels{lab} = sprintf('BER, %dMHz RC filter (simulation)',fp(k)/1e6);
        lab = lab+1;
        plot(SNR_th,log10(BER_rc_fit{k}(db2pow(SNR_th))),'color',pl.Color);
        labels{lab} = sprintf('BER, %dMHz RC filter (fit)',fp(k)/1e6);
        lab = lab+1;
        xline(SNRtarg_rc(k),'--','color',pl.Color);
        labels{lab} = sprintf('target E_b/N_0, %dMHz RC filter (fit)',fp(k)/1e6);
        lab = lab+1;
    end
    if BpS == 1
        xlim([6 13]);
    elseif BpS == 2
        xlim([10 20]);
    end
    grid on;
    title({'BER: matched vs. RC filter';sprintf('(%d-PAM)',M)});
    xlabel('E_b/N_0 [dB]');
    ylabel('log_{10}(BER)');
    legend(labels,'location','southwest');
    saveas(gcf,sprintf('matched_vs_rc_%dpam.svg',M));

    %% SER vs. BER: matched filter
    figure;
    hold on;
    pl = plot(SNR_th,log10(SER_matched_th));
    plot(SNR,log10(SER_matched),'o','color',pl.Color);
    pl = plot(SNR_th,log10(BER_matched_th));
    plot(SNR,log10(BER_matched),'o','color',pl.Color);
    if BpS == 1
        xlim([1 10]);
    elseif BpS == 2
        xlim([1 14]);
    end
    grid on;
    if mapping == "gray"
        title({'SER vs. BER: matched filter';sprintf('(%d-PAM, Gray mapping, %s symbols)',M,symbols)});
    else
        title({'SER vs. BER: matched filter';sprintf('(%d-PAM, standard mapping, %s symbols)',M,symbols)});
    end
    xlabel('E_b/N_0 [dB]');
    ylabel('log_{10}(ER)');
    labels = {'SER (theory)','SER (simulation)','BER (theory)','BER (simulation)'};
    legend(labels,'location','southwest');
    saveas(gcf,sprintf('ser_vs_ber_%s_%s_%dpam.svg',mapping,symbols,M));
else
    SER_matched
    BER_matched
    BER_rc
end